# Talk Access Code School - Jovo Frameworks

One Paragraph of project description goes here

### Prérequis / Installation

Voir la rediffusion du talk ci-dessous :

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)

## Technologies utilisées

* [Jovo Framework](https://www.jovo.tech/) - Jovo Framework

## Auteur

* **Anthony Tournier** - [GitLab](https://gitlab.com/synyster0fa7x)

## Licence

Ce projet est sous licence MIT - voir le fichier [LICENSE.md](LICENSE.md) pour davantages de renseignements.