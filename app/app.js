'use strict';

// =================================================================================
// App Configuration
// =================================================================================

const {App} = require('jovo-framework');

const config = {
    logging: false,
};

const app = new App(config);


// =================================================================================
// App Logic
// =================================================================================

app.setHandler({
    'LAUNCH': function() {
        this.ask('Que puis-je faire pour toi ?');
    },
    'ProductionToday': function(date) {

        if (typeof date.value == 'undefined') {
            var askedDate = new Date();
        } else {
            var askedDate = new Date(Date.parse(date.value));
        }

        var day = askedDate.getDay();

        if (day == 0 || day == 6) { // weekd-end
            this.tell("NON ! PAS LE WEEK-END !");
        } else if (day == 4) { // jeudi
            this.tell("Le matin oui, mais l'après-midi, bof bof...");
        } else if (day == 5) { // vendredi
            this.tell("Certainement pas.");
        } else {
            // lundi mardi mercredi
            this.tell('Pas de soucis !');
        }
    }

});

module.exports.app = app;
