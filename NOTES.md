# Powerpoint

## Slide 1 - Définition

## Slide 2 - Statistiques marché
Prédictions : 
* +29% d'utilisateurs en 2021.
* +46% du marché global d'ici 2021

Utilités principales : 
* utile si main occupée / vision obstruée
* résultats plus rapides
* difficultés d'utiliser d'autres moyens de commande

## Slide 3 : Acteurs principaux (grand public)
* Google : Google Assistant, Google Home mini, Google home max...    
    * arrivée en france en 2016/2017
* Amazon : Gamme Echo (Echo, Echo Sport, Echo Dot)...
    * arrivée en france en 13 juin 2018

## Slide 4 : Comment fonctionne une requête passée par un assistant vocal.


# LIVECODING

## Installation projet
```bash
cd /tmp
jovo new acs-alexa
cd acs-alexa
code .
```

## Création application alexa (skill) dans la console developpeur
* Initialisation skill alexa : `jovo init alexaSkill`
* Si action Google : `jovo init googleAction`

* https://developer.amazon.com/alexa/console/ask#
* Nom : acs
* Configuration de models/fr-FR.json pour faire un hello world (bien mettre AMAZON.FirstName comme slot type)
* `jovo build`
* Configuration de l'ID de la skill dans `/platforms/.ask/config`
* `jovo deploy`





# Pense-bête

* Installation de ASK (Alexa Skill Command) : https://developer.amazon.com/fr/docs/smapi/quick-start-alexa-skills-kit-command-line-interface.html

* Si jovo build retourne une erreur est incorrect : 
`ask api update-model -s amzn1.ask.skill.7c6f97ec-c306-4d7c-8700-8c87f3b86a2e -l fr-FR -f platforms/alexaSkill/models/fr-FR.json`

* Slots types
https://developer.amazon.com/fr/docs/custom-skills/slot-type-reference.html#availability